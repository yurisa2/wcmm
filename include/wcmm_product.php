<?php
class wcmmProduct extends MMProduct
{
  public function __construct()
  {
    parent::__construct();
    $this->wooCommerceProduct = new wooCommerceProduct;
  }

  public function wcmmUpdateProduct($productSku)
  {
    $productId = $this->wooCommerceProduct->wooCommerceGetProductId($this->skuRefactore($productSku));
    $productInfo = $this->wooCommerceProduct->wooCommerceGetProduct($productId);

    $additional_settings = $productInfo->price * SETTINGS_PRICE_MULTIPLICATION + SETTINGS_PRICE_ADDITION;
    $productEntity['price'] = round($productInfo->price * (1+COMMISSION) + $additional_settings,2);
    $productEntity['qty'] = $productInfo->stock_quantity;

    $updateProductReturn = $this->mmUpdateProductBulk($productSku,$productEntity['price'],$productEntity['qty']);

    if(!isset($updateProductReturn->data)) return false;

    return true;
  }

  public function skuRefactore($productSku)
  {
    $start = substr($productSku,0,2);
    $midle = substr($productSku,2,2);
    $end   = substr($productSku,4);

    return "$start-$midle-$end";
  }
}
?>
