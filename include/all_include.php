<?php
$prefix_mm = 'include/apimmwrapperphp/';
$prefix_wc = 'include/apiwoocommercephp/';

require_once 'include/config.php';
require_once 'include/defines.php';

require_once 'include/apimmwrapperphp/include/all_include.php';
require_once 'include/sa2_flux/include/flux.php';
require_once 'include/apiwoocommercephp/include/all_include.php';


require_once 'include/wcmm_product.php';
require_once 'include/wcmm_order.php';

require_once 'include/event_base.php';
require_once 'include/error_handling.php';
require_once 'include/log.php';

require_once 'PHPMailer/src/Exception.php';
require_once 'PHPMailer/src/PHPMailer.php';
require_once 'PHPMailer/src/SMTP.php';

 ?>
