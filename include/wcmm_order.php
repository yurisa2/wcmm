<?php
class wcmmOrder extends MMOrder
{
  public function __construct()
  {
    parent::__construct();
    $this->wooCommerceOrder = new wooCommerceOrder;
    $this->wooCommerceProduct = new wooCommerceProduct;
    $this->wooCommerceCustomer = new wooCommerceCustomer;
  }

  public function wcmagaluCreateOrder($orderId)
  {
    $orderData = $this->magaluGetOrder($orderId);

    if(count($orderData->Payments) > 1 || count($orderData->Products) > 1) {
      $error = new error_handling("WCMAGALU: Pedido com mais de um produto e/ou pagamento","Pedido: $orderId" , "Por favor acompanhar pessoalmente o pedido" , "warning pedido");
      $error->send_error_email();
      $error->execute();
      echo '<br>Pedido com mais de um produto e/ou pagamento';
      return false;
    }

    $this->normalizeCustomerData($orderData);

    $customerList = $this->wooCommerceCustomer->WooCommerceGetCustomerList(['search'=>$this->normalizeCustomerData->username]);

    if(empty($customerList)) $orderData[0]->wcCustomerId = $this->wooCommerceCustomer->wooCommerceCreateCustomer($this->normalizeCustomerData)->id;
    else $orderData->wcCustomerId = $customerList[0]->id;

    $this->normalizeOrderData($orderData);
    try {
      $createOrder = $this->wooCommerceOrder->wooCommerceCreateOrder($this->normalizeOrderData);
    } catch(Exception $exception) {
      $error = new error_handling("WCMAGALU: Erro ao criar pedido no WooCommerce", (string)$exception->getMessage(), "Pedido: $orderId", "Erro pedido");
      $error->send_error_email();
      $error->execute();
      echo '<br>'.$exception->getMessage();
      return false;
    }

    // $labelUrl = $this->meliGetOrderLabel($this->normalizeCustomerData->shippingId, json_encode($orderId));

    // copy($labelUrl,str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/conectores/pedidos/'.json_encode($orderId).".pdf");
    // pega o nome do comprador

    $nome = $this->normalizeCustomerData->firstName.' '.$this->normalizeCustomerData->lastName ;
    $log = new log("Novo Pedido Magalu", "Numero do Pedido: ".json_encode($orderId), "Comprador: $nome", "nova compra");
    $log->log_email = true;
    $log->mensagem_email = "Nova compra Magalu";
    $log->log_email = true;
    $log->email_novacompra = true;
    $log->dir_file = "log/log.json";
    $log->log_files = true;
    $log->send_log_email();
    $log->execute();

    $orderStatusEntities = ["IdOrder" => $orderId,"OrderStatus" => "PROCESSING"];
    $this->magaluUpdateOrder($orderId,$orderStatusEntities);

    return true;
  }

  protected function normalizeCustomerData($orderData)
  {
    $nameLastname = ucwords(strtolower($orderData->CustomerPfName));
    $name = explode(' ', $nameLastname);
    $lastname = array_splice($name, -1);
    $name = implode(' ',$name);
    $lastname = implode(' ',$lastname);

    $username = explode(' ', strtolower($orderData->CustomerPfName));
    array_splice($username, 1);
    $username = strtolower(implode('',$username).'.'.$lastname);
    $email = $orderData->CustomerMail;
    $phone = trim($orderData->TelephoneMainNumber);
    $cpf =  $orderData->CustomerPfCpf;
    $this->normalizeCustomerData->email = $email;
    $this->normalizeCustomerData->firstName = "MGL ".$name;
    $this->normalizeCustomerData->lastName = $lastname;
    $this->normalizeCustomerData->username = $username;

    $street = $orderData->DeliveryAddressStreet;
    $number =$orderData->DeliveryAddressNumber;
    $neighborhood = $orderData->DeliveryAddressNeighborhood;
    $complement = $orderData->DeliveryAddressAdditionalInfo;
    $postcode = $orderData->DeliveryAddressZipcode;
    $city = $orderData->DeliveryAddressCity;
    $state = $orderData->DeliveryAddressState;
    $country = 'BR';


    if(!empty($neighborhood)) $neighborhoodAddress = '-'.$neighborhood;

    $this->normalizeCustomerData->billingFirstName = $this->normalizeCustomerData->firstName;
    $this->normalizeCustomerData->billingLastname = '';
    $this->normalizeCustomerData->billingAddress = $street.', '.$number.$neighborhoodAddress;
    $this->normalizeCustomerData->billingAddress2 = $complement;
    $this->normalizeCustomerData->billingStreet = $street;
    $this->normalizeCustomerData->billingNumber = $number;
    $this->normalizeCustomerData->billingNeighborhood = $neighborhood;
    $this->normalizeCustomerData->billingCity = $city;
    $this->normalizeCustomerData->billingState = $state;
    $this->normalizeCustomerData->billingCep = $postcode;
    $this->normalizeCustomerData->billingCountry = $country;
    $this->normalizeCustomerData->billingEmail = $email;
    $this->normalizeCustomerData->billingPhone = $phone;
    $this->normalizeCustomerData->shippingFirstName = $this->normalizeCustomerData->firstName;
    $this->normalizeCustomerData->shippingLastname = '';
    $this->normalizeCustomerData->shippingAddress = $street.', '.$number.$neighborhoodAddress;
    $this->normalizeCustomerData->shippingAddress2 = $complement;
    $this->normalizeCustomerData->shippingStreet = $street;
    $this->normalizeCustomerData->shippingNumber = $number;
    $this->normalizeCustomerData->shippingNeighborhood = $neighborhood;
    $this->normalizeCustomerData->shippingCity = $city;
    $this->normalizeCustomerData->shippingState = $state;
    $this->normalizeCustomerData->shippingCep = $postcode;
    $this->normalizeCustomerData->shippingCountry = $country;
    $this->normalizeCustomerData->cpf = $cpf;

    // var_dump($this->normalizeCustomerData);         //DEBUG
    // exit;                                           //DEBUG
    return $this->normalizeCustomerData;
  }

  protected function normalizeOrderData($orderData)
  {

    foreach ($orderData->Payments as $key => $value) {

      $paymentType = $value->Name;
      $parcels = $value->Installments;

      $totalPaid = $value->Amount;
    }

    foreach ($orderData->Products as $key => $value) {
      $subtotal = (float)$value->Price*(int)$value->Quantity;
      $productSku = $value->IdSku;

      $productId = $this->wooCommerceProduct->wooCommerceGetProductId($productSku);
      $productQty = $value->Quantity;

      $this->normalizeOrderData->productItems[] = [
        'product_id' => $productId,
        'quantity' => $productQty,
        // 'total' => $value->quantity * $value->unit_price
        'total' => $value->Price
      ];
    }

    $shippingCost = $orderData->TotalFreight;

    // $totalPaid = $totalPaid - $commissionTotal - $shippingCost;
    // $labelUrl = $this->meliGetOrderLabel($this->normalizeCustomerData->shippingId, json_encode($orderId));

    $this->normalizeOrderData->meta_data[] = ['key' => 'Tipo de pagamento', 'value' => $paymentType];

    $this->normalizeOrderData->meta_data[] = ['key' => 'totalToPay', 'value' => $totalPaid];
    $this->normalizeOrderData->meta_data[] = ['key' => 'Parcelas', 'value' => $parcels];

    $this->normalizeOrderData->shippingName = $this->normalizeCustomerData->firstName;
    $this->normalizeOrderData->shippingLastname = $this->normalizeCustomerData->lastName;
    $this->normalizeOrderData->shippingAddress = $this->normalizeCustomerData->shippingAddress;
    $this->normalizeOrderData->shippingAddress2 = $this->normalizeCustomerData->shippingAddress2;
    $this->normalizeOrderData->shippingCity = $this->normalizeCustomerData->shippingCity;
    $this->normalizeOrderData->shippingState = $this->normalizeCustomerData->shippingState;
    $this->normalizeOrderData->shippingCep = $this->normalizeCustomerData->shippingCep;
    $this->normalizeOrderData->shippingCountry = $this->normalizeCustomerData->shippingCountry;

    $this->normalizeOrderData->billingName = $this->normalizeCustomerData->firstName;
    $this->normalizeOrderData->billingLastname = $this->normalizeCustomerData->lastName;
    $this->normalizeOrderData->billingAddress = $this->normalizeCustomerData->billingAddress;
    $this->normalizeOrderData->billingAddress2 = $this->normalizeCustomerData->billingAddress2;
    $this->normalizeOrderData->billingCity = $this->normalizeCustomerData->billingCity;
    $this->normalizeOrderData->billingState = $this->normalizeCustomerData->billingState;
    $this->normalizeOrderData->billingCep = $this->normalizeCustomerData->billingCep;
    $this->normalizeOrderData->billingCountry = $this->normalizeCustomerData->billingCountry;
    $this->normalizeOrderData->billingEmail = $this->normalizeCustomerData->billingEmail;

    $this->normalizeOrderData->billingPhone = $this->normalizeCustomerData->billingPhone;

    $this->normalizeOrderData->shippingTotal = 0;

    $this->normalizeOrderData->customerId = $orderData->wcCustomerId;

    $this->normalizeOrderData->meta_data[] = ['key' => '_billing_cpf', 'value' => $this->normalizeCustomerData->cpf];
    $this->normalizeOrderData->meta_data[] = ['key' => '_shipping_number', 'value' => $this->normalizeCustomerData->shippingNumber];
    $this->normalizeOrderData->meta_data[] = ['key' => '_shipping_neighborhood', 'value' => $this->normalizeCustomerData->shippingNeighborhood];
    $this->normalizeOrderData->meta_data[] = ['key' => '_billing_neighborhood', 'value' => $this->normalizeCustomerData->billingNeighborhood];
    $this->normalizeOrderData->meta_data[] = ['key' => '_billing_number', 'value' => $this->normalizeCustomerData->billingNumber];


    $this->normalizeOrderData->meta_data[] = ['key' => 'orderId', 'value' => $orderData->IdOrder];
    $this->normalizeOrderData->meta_data[] = ['key' => 'subtotal', 'value' => $subtotal];
    // $this->normalizeOrderData->meta_data[] = ['key' => 'discount', 'value' => 0];
    // $this->normalizeOrderData->meta_data[] = ['key' => 'commission', 'value' => $commissionTotal];
    $this->normalizeOrderData->meta_data[] = ['key' => 'shipping', 'value' => $shippingCost];

    // $this->normalizeOrderData->meta_data[] = ['key' => 'etiqueta', 'value' => base64_encode(file_get_contents($labelUrl))];

    // var_dump($this->normalizeOrderData);         //DEBUG
    // exit('OrderData');                                           //DEBUG
    return $this->normalizeOrderData;
  }

  public function wcmeliOrderExists($mktplaceOrderId,$limit)
  {
    try {
      $salesOrder = $this->wooCommerceOrder->wooCommerceGetOrders(['order'=>'desc','per_page'=>$limit]);

      if(count($salesOrder) <= $limit) {
        foreach ($salesOrder as $key => $value) {
          $orderData = $this->wooCommerceOrder->wooCommerceGetOrder($value->id);
          foreach ($orderData->meta_data as $key => $value) {

            if($value->key == "orderId" && $value->value == $mktplaceOrderId) return true;
          }

        }
        return false;
      }

      foreach ($salesOrder as $key => $value) {
        if($key < count($salesOrder)-$limit) $last_orders[] = $value;
      }
      foreach ($last_orders as $key => $value) {
        $orderData = $this->wooCommerceOrder->wooCommerceGetOrder($value->id);
        foreach ($orderData->meta_data as $key => $value) {
          if($value->value == $mktplaceOrderId) return true;
        }
      }
      return false;
    } catch(Exception $e) {
      $error = new error_handling("WCB2W: Erro ao buscar pedidos","Tentativa falha de buscar a lista de pedidos", "Erro<br>".$e->getMessage(), "Erro Pedido");
      $error->send_error_email();
      $error->execute();
      exit("Skyhub com problemas. Não foi possível retornar os pedidos");
    }
  }

  public function skuRefactore($productSku)
  {
    $start = substr($productSku,0,2);
    $midle = substr($productSku,2,2);
    $end   = substr($productSku,4);

    return "$start-$midle-$end";
  }
}
?>
